var STORAGE_KEY = "todos-vuejs-demo";
var todoStorage = {
  fetch: function() {
    var todos = JSON.parse(localStorage.getItem(STORAGE_KEY) || "[]");
    todos.forEach(function(todo, index) {
      todo.id = index;
    });
    todoStorage.uid = todos.length;
    return todos;
  },
  save: function(todos) {
    localStorage.setItem(STORAGE_KEY, JSON.stringify(todos));
  }
};

const app = new Vue({
  el: "#app",
  data: {
    todos: [],
    options: [
      // 状態のオブジェクト
      { value: -1, label: "すべて", class: "all" },
      { value: 0, label: "作業中", class: "now" },
      { value: 1, label: "レビュー待ち", class: "review" },
      { value: 2, label: "完了", class: "finish" }
    ],
    current: -1, // 初期値は「-1(すべて)」とする
    editedTodo: null
  },
  computed: {
    computedTodos: function() {
      // current と stateが一致するものを絞り込む
      // currentがマイナス値ならすべて表示
      return this.todos.filter(function(el) {
        return this.current < 0 ? true : this.current === el.state;
      }, this);
    },
    labels() {
      // 状態変更ボタンが数字で表示されるので日本語に
      return this.options.reduce(function(a, b) {
        return Object.assign(a, { [b.value]: b.label });
      }, {});
    },
    stateClass() {
      // 各状態変更ボタンのクラス
      return this.options.reduce(function(a, b) {
        return Object.assign(a, { [b.value]: b.class });
      }, {});
    }
  },
  watch: {
    // todosをウォッチして変更があった場合
    // ローカルストレージに保存
    todos: {
      handler: function(todos) {
        todoStorage.save(todos);
      },
      deep: true
    }
  },
  created() {
    // インスタンス作成時にfetch()
    this.todos = todoStorage.fetch();
  },
  methods: {
    // ToDo追加
    doAdd: function(event, value) {
      var comment = this.$refs.comment;
      var category = this.$refs.category;
      // 入力がなかればダイアログでお知らせ
      if (!comment.value.length) {
        return alert("タスクを入力してください");
      } else if (!category.value.length) {
        return alert("カテゴリを入力してください");
      }
      // タスク作成日付
      var today = new Date();
      var year = today.getFullYear();
      var month = today.getMonth() + 1;
      var day = today.getDate();
      var created_at =
        year + "." + ("0" + month).slice(-2) + "." + ("0" + day).slice(-2);
      var updated_at = updated_at;
      // 新しいToDoオブジェクトをtodosへpush
      this.todos.push({
        id: todoStorage.uid++,
        created_at: created_at,
        updated_at: updated_at,
        category: category.value,
        comment: comment.value,
        state: 0 // 状態はデフォで「作業中=0」とする
      });
      // フォームをリセット
      comment.value = "";
      category.value = "";
    },
    // フォームをリセット
    formReset() {
      var comment = this.$refs.comment;
      var category = this.$refs.category;
      comment.value = "";
      category.value = "";
    },
    // 編集用テキストエリア展開
    doEditShow: function(item) {
      this.beforeEditCache = item.comment;
      this.editedTodo = item;
    },
    // 編集内容と更新日時の上書き
    doEdit: function(item) {
      if (!this.editedTodo) {
        return;
      }
      this.editedTodo = null;
      const comment = item.comment.trim();
      if (comment) {
        item.comment = comment;
      } else {
        this.doRemove(item);
      }
      var today = new Date();
      var year = today.getFullYear();
      var month = today.getMonth() + 1;
      var day = today.getDate();
      var hour = today.getHours();
      var minutes = today.getMinutes();
      var updated_at =
        year +
        "." +
        ("0" + month).slice(-2) +
        "." +
        ("0" + day).slice(-2) +
        " " +
        ("0" + hour).slice(-2) +
        ":" +
        ("0" + minutes).slice(-2);
      item.updated_at = updated_at;
    },
    // 編集キャンセル
    doEditCansel(item) {
      this.editedTodo = null;
      item.comment = this.beforeEditCache;
    },
    // 状態変更
    doChangeState: function(item) {
      item.state++;
      var today = new Date();
      var year = today.getFullYear();
      var month = today.getMonth() + 1;
      var day = today.getDate();
      var hour = today.getHours();
      var minutes = today.getMinutes();
      var updated_at =
        year +
        "." +
        ("0" + month).slice(-2) +
        "." +
        ("0" + day).slice(-2) +
        " " +
        ("0" + hour).slice(-2) +
        ":" +
        ("0" + minutes).slice(-2);
      item.updated_at = updated_at;
      if (item.state > 2) {
        // 状態は0,1,2までなので3以上は0にリセット
        item.state = 0;
      }
    },
    // タスク削除
    doRemove: function(item) {
      var index = this.todos.indexOf(item);
      this.todos.splice(index, 1);
    },
    // 全タスク削除
    doAllreset: function(todos) {
      if (confirm("すべてのタスクを削除しますか？")) {
        this.todos = [];
      }
    }
  },
  directives: {
    "todo-focus"(element, binding) {
      if (binding.value) {
        element.focus();
      }
    }
  }
});
